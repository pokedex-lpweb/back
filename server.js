var express = require('express');
var fs = require('fs');
var cors = require('cors');
var bodyParser = require('body-parser');
var bcrypt = require('bcryptjs');
var saltRounds = 10;
var db = require('mongoose');


db.connect('mongodb://kevin:123Soleil@92.222.69.165/pokedex',{ useNewUrlParser: true });
var Schema = db.Schema;

var userSchema = new Schema({
    "name":String,
    "surname":String,
    "mail":{ type : String , unique : true},
    "pass":String,
    "money":Number,
    "pokemons":[
        Number
    ]
});
var User = db.model('User', userSchema);

var regMail = new RegExp("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,}$");

var app = express();

app.use(
    cors()
    ).use(bodyParser.urlencoded({
        extended: false
    })).use(
        bodyParser.json()
    ).get('/', function (req, res) {
        res.write('hello my darkness friend');
        // User.createCollection().then(function(collection) {
        //     console.log('Collection is created!');
        // });  For init only
        res.send();
    }).post('/user/signup', function (req, res) {
        console.log(req.body);
        let name = req.body.lastname;
        let surname = req.body.firstname;
        let mail = req.body.mail;
        let pass = req.body.password;
        let hashpass = '';
        console.log(regMail.test(mail));
        if(regMail.test(mail)){//
            User.findOne({ "mail": mail }, function (err, obj) {
                if (obj != undefined) {
                    let content = {
                        "success":false,
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(content));
                }else{
                    bcrypt.hash(pass, saltRounds, function(err, hash) {
                        hashpass = hash;
                        usr = new User({
                            "name":name,
                            "surname":surname,
                            "mail":mail,
                            "pass":hashpass,
                            "money":100,
                            "pokemons":[]
                        });
                        let token = '';
                        usr.save(function (err,obj) {
                            if(err){
                                console.log(err);
                            }
                            token = obj.id;
                            let content = {
                                "success":true,
                                "token":token
                            }
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.send(JSON.stringify(content));
                        });
                    });
                }
            });
        }else{
            let content = {
                "success":false,
            }
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(content));
        }  
    }).post('/user/signin', function (req, res) {
        let mail = req.body.mail;
        let pass = req.body.password;
        if(regMail.test(mail)){
            User.findOne({ "mail": mail }, function (err, obj) {
                if(obj){
                    let bddpass = obj.pass;
                    bcrypt.compare(pass, bddpass, function(err, result) {
                        if(result){
                            let token = obj.id;
                            let content = {
                                "success":true,
                                "token":token,
                                "lastname":obj.name,
                                "firstname":obj.surname,
                                "money":obj.money,
                                "mail":obj.mail
                            };
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.send(JSON.stringify(content));
                        }else{
                            let content = {
                                "success":false
                            }
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.send(JSON.stringify(content));
                        }
                    });
                }else{
                    let content = {
                        "success":false
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(content));
                }
                
            });
        }else{
            let content = {
                "success":false,
            }
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(content));
        }
    }).post('/user/remove', function (req, res) {
        let mail = req.body.mail;
        let pass = req.body.password;

        User.findOne({ "mail": mail }, function (err, obj) {
            if(obj){
                let bddpass = obj.pass;

                console.log(bddpass);
                bcrypt.compare(pass, bddpass, function(err, result) {
                    if(result){
                        let token = obj.id;
                        User.remove({"_id":token},function (err){
                            if(err){
                                let content = {
                                    "success":false
                                }
                                res.statusCode = 200;
                                res.setHeader('Content-Type', 'application/json');
                                res.send(JSON.stringify(content));
                            }else{
                                let content = {
                                    "success":true,
                                };
                                res.statusCode = 200;
                                res.setHeader('Content-Type', 'application/json');
                                res.send(JSON.stringify(content));
                            }
                        })
                    }else{
                        let content = {
                            "success":false
                        }
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify(content));
                    }
                });
            }else{
                let content = {
                    "success":false
                }
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify(content));
            }
            
        });
    }).put('/pkm', function (req, res) {
        let id = req.body.id;
        let token = req.body.token;

        User.findById(token, function (err, obj) {
            let pkmlist = obj.pokemons;
            pkmlist.push(id);
            User.update({"_id":token},{"pokemons":pkmlist},function (err, obj){
                if(err){
                    let content = {
                        "success":false
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(content));
                }else{
                    let content = {
                        "success":true
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(content));
                }
            });
        });
    }).get('/pkm/:token', function (req, res) {
        let token = req.params.token;

        User.findById(token, function (err, obj) {
            let pkmlist = obj.pokemons;
            if(err){
                let content = {
                    "success":false
                }
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify(content));
            }else{
                let content = {
                    "success":true,
                    "pokemons": pkmlist
                }
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify(content));
            }
        });
    }).post('/pkm', function (req, res) {
        let id = req.body.id;
        let token = req.body.token;

        User.findById(token, function (err, obj) {
            let pkmlist = obj.pokemons;
            pkmlist = pkmlist.filter(item => item != id);
            User.update({"_id":token},{"pokemons":pkmlist},function (err, obj){
                console.log(obj);
                if(err){
                    let content = {
                        "success":false
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(content));
                }else{
                    let content = {
                        "success":true
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(content));
                }
            });
        });
    }).post('/money', function (req, res) {
        let amount = req.body.amount;
        let token = req.body.token;

        User.findById(token, function (err, obj) {
            let money = obj.money;
            money = money + parseInt(amount)
            User.update({"_id":token},{"money":money},function (err, obj){
                if(err){
                    let content = {
                        "success":false
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(content));
                }else{
                    let content = {
                        "success":true
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(content));
                }
            });
        });
    }).use('/pp',
        express.static('public/pp')
    )

app.listen(13370);