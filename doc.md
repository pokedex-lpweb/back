
==== DOCS API Pokedex Data Persistence



== Signup
POST /user/signup
entry:
{
	"name":"kevin",
	"surname":"RIOU",
	"mail":"riou.kkevin@gmail.com",
	"password":"motdepasse"
}
out:
{
  "success":true,
  "token":token
}



== Signin
POST /user/signin
entry:
{
	"mail":"riou.kkevin@gmail.com",
	"password":"motdepasse"
}
out:
{
  "success": true,
  "token": token,
  "lastname": lastname,
  "firstname": firstname,
  "money": 100,
  "mail": email
}



== Delete account
DELETE /user/delete
entry:
{
	"mail":"riou.kkevin@gmail.com",
	"password":"motdepasse"
}
out:
{
  "success":true,
  "token":token
}



== List all pokemon of one user
GET /pkm
entry:
{
	"token":"5c3c474911feae18efa4d03a"
}
out:
{
  "success": true,
  "pokemons": []
}



== Add pokemon
PUT /pkm
entry: 
{
	"id":"22",
	"token":"5c3c474911feae18efa4d03a"
}
out:
{
  "success": true
}



== Suppress pokemon
DELETE /pkm
entry: 
{
	"id":"22",
	"token":"5c3c474911feae18efa4d03a"
}
out:
{
  "success": true
}



== Modify Money
/money
entry:
{
	"amount":"-2",
	"token":"5c3d9d3607ff1521f2c67ab9"
}
out:
{
  "success": true
}